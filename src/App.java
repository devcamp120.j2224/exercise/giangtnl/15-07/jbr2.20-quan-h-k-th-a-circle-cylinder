public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Giang", "giang@gmail.com", 'f');
        Author author2 = new Author("Linh", "linh@gmail.com", 'm');
        System.out.println(author1.toString());
        System.out.println(author2.toString());
        Book book1 = new Book("Cỏ và Lá", author1 , 15000);
        Book book2 = new Book("Em và Trịnh", author1 , 20000, 2);
        System.out.println(book1.toString());
        System.out.println(book2.toString());
    }
}
